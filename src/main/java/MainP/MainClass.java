package MainP;
import Presentation.*;
import Business.*;
import Business.MenuItem;
import Data.Serializator;
import Data.UsersList;


import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class MainClass {
    public static void main(String[] args) throws Exception {

        DeliveryService del = DeliveryService.getDelivery();
        LogInView view = new LogInView(del);
        Controller con = new Controller(view, del);
        view.setVisible(true);
    }
}
