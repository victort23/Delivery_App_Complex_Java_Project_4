package Data;

import java.io.Serializable;
/////////////////////////////////////DONE
public class User implements Serializable {

    //Clasa de baza pt creearea user-ilor
    private UserLevel userLvl;
    private String usname;    private String pass;

    public User(String user, String pass, UserLevel userLvl) {
        this.userLvl = userLvl;
        this.usname = user;     this.pass = pass;

    }
    //Constructorii pentru operatiile cu useri
    public void setUserLevel(UserLevel userLl) {
        this.userLvl = userLvl;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public UserLevel getUserLvl() {
        return userLvl;
    }
    public String getPass() {
        return pass;
    }
    public void setUsername(String username) {
        this.usname = username;
    }
    public String getUsername() {
        return usname;
    }
    @Override
    public String toString() {
        return "[USER]" +
                "Username: " + usname +
                ", password: " + pass +
                ", Role: " + userLvl.toString();
    }








}
