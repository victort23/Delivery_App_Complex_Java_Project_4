package Data;
import java.io.Serializable;
import java.io.IOException;

////////////////////////////////////////////DONE
public enum UserLevel implements Serializable {
    EMPLOYEE,
    CL,
    ADMIN;

    public static UserLevel getLvl(String role) throws IOException {
        if(role.contentEquals("administrator")){
            return ADMIN;
        } else if(role.contentEquals("employee")){
            return EMPLOYEE;
        }
        else if(role.contentEquals("client")) {
            return CL;
        }
        else
            throw new IOException();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
