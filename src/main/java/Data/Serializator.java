package Data;

import java.io.*;
import Business.DeliveryService;
//////////////////////////////////////DONE
public class Serializator {
    //Codul pentru deserealizarea datelor aplicatiei

    public static DeliveryService deserialize(String fName)
    {
        DeliveryService delServ = null;
        try{
            FileInputStream file = new FileInputStream(fName);
            ObjectInputStream in = new ObjectInputStream(file);            delServ = (DeliveryService)in.readObject();
            in.close(); file.close();
            return delServ;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    //Codul pentru serializarea datelor aplicatiei
    public static void serialize(Serializable o) throws IOException {
        FileOutputStream f = new FileOutputStream("DeliveryApp.ser");
        ObjectOutputStream outputStream = new ObjectOutputStream(f);
        outputStream.writeObject(o);
        outputStream.close();   f.close();
    }
}
