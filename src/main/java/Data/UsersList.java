package Data;
import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.ArrayList;
/////////////////////////////////////////////////////////DONE

public class UsersList implements Serializable{
    private ArrayList<User> userii;

    public UsersList() throws IOException {
        File usF = new File("users.txt");
        Scanner read = new Scanner(usF);
        userii = new ArrayList<>();   int number = 1;
        String user = "",pass = "",role = "";
        while(read.hasNextLine())
        {
            String da = read.nextLine();
            if(!da.isEmpty()) {
                for (String input : da.split(" ")) {
                    switch (number) {
                        case 1:
                            user = input;
                        case 2:
                            pass = input;
                        default:
                            role = input;
                    }
                    number++;
                }
                userii.add(new User(user, pass, UserLevel.getLvl(role)));
            }
        }
        for(User u: userii)
        {
            System.out.println(u);
        }
    }
    //Verificam datele de introducere a user-ului
    public UserLevel verifUser(String user, String pass) throws NoSuchElementException
    {

        for(User u: userii)
        {
            if(u.getUsername().contentEquals(user))
            {
                if(u.getPass().contentEquals(pass))
                    return u.getUserLvl();
                else
                    throw new NoSuchElementException("Parola gresita");
            }
        }
        //Nu exista userul
        throw new NoSuchElementException("User necunoscut");
    }
    //Adaugam user=ul nou
    public void addUser(String username, String password, String role) throws Exception {
        if(UserLevel.getLvl(role.toLowerCase()) != null && !dejaExista(username)){
            userii.add(new User(username, password, UserLevel.getLvl(role.toLowerCase())));
            FileWriter f = new FileWriter("users.txt", true);
            BufferedWriter bw = new BufferedWriter(f); PrintWriter wr = new PrintWriter(bw);
            wr.println(username + " " + password + " " + role.toLowerCase());     wr.close();
            System.out.println("Un utilizator a fost adaugat");
        }
        else
        {
            throw new Exception("User-ul deja exista");
        }
    }

    //Verificam daca exista admin ul
    public boolean verifExistaAdmin()
    {
        for(User user: userii){
            if(user.getUserLvl() == UserLevel.ADMIN)
                return true;
        }
        return false;
    }
    //Obtinem clientii din lista cu stream-uri
    public ArrayList<User> getClients()
    {
        return userii.stream().filter(user -> user.getUserLvl() == UserLevel.CL).collect(Collectors.toCollection(ArrayList::new));
    }
    //Verificam existenta tuturor
    private boolean dejaExista(String username)
    {
        for(User u: userii)
        {
            if(u.getUsername().contentEquals(username))
                return true;
        }
        return false;
    }
    //Verificam daca e goala lista de useri
    public boolean isEmpty()
    {
        return userii.isEmpty();
    }
    //Obtine user Id
    public int getUserId(String username)
    {
        for(User u: userii)
        {
            if(u.getUsername().contentEquals(username))
            {
                return userii.indexOf(u);
            }
        }
        return -1;
    }
    public ArrayList<User> getUsers()
    {
        return userii;
    }





}
