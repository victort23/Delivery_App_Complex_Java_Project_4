package Business;

import java.io.File;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

public class Order implements Serializable
{
    private int ordId;    private int ordPrice;

    private int clId;    private Date ordDate;


    public Order(int orId, int clId, Date d, int ordPrice)
    {
        this.clId = clId;
        this.ordId = ordId;
        ordDate = d;
        this.ordPrice = ordPrice;
    }




    //Getters and setters pentru operatii cu comenzi

    public void setOrderDate(Date orderDate)
    {
        this.ordDate = orderDate;
    }

    public int getOrderPrice()
    {
        return ordPrice;
    }
    public int getOrderId()
    {
        return ordId;
    }
    public void setOrderId(int orderId)
    {
        this.ordId = orderId;
    }
    public Date getOrderDate()
    {
        return ordDate;
    }
    public void setOrderPrice(int ordPrice)
    {
        this.ordPrice = ordPrice;
    }
    public void setClientId(int clientId)
    {
        this.clId = clientId;
    }
    public int getClientId()
    {
        return clId;
    }
    public void printBill( String usname,ArrayList<MenuItem> list) throws IOException
    {
        String fileName = "bill.txt";
        SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        File file = new File(fileName);   file.createNewFile();



        FileWriter fw = new FileWriter(file, true);
        fw.append("THE ORDER " + ordId + " WAS MADE BY THE CLIENT : " + usname + "\n");
        for(MenuItem item: list){
            fw.append(item.toString() + "\n");
        }
        fw.append("TOTAL PRICE IS: " + ordPrice + "\n");        formatter = new SimpleDateFormat("HH:mm, dd.MM.yyyy");

        fw.append("ORDER MADE AT: " + formatter.format(ordDate) + "\n");       fw.close();

    }

    public boolean checkDate(String hbs, String has)
    {
        int HB = Integer.parseInt(hbs);        int HA = Integer.parseInt(has);
        SimpleDateFormat formatter = new SimpleDateFormat("HH");      int Oh = Integer.parseInt(formatter.format(ordDate));
        return HB <= Oh && Oh <= HA;
    }

    public boolean checkDateProduct(String date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        String orderD = formatter.format(ordDate);
        int ordDay = 0,ordMth = 0 ,ordYear = 0,koef=0, yr = 0, mth = 0,
                day = 0;

        for(String val: orderD.split("."))
        {
            if(koef == 0)

                ordDay = Integer.parseInt(val);

            else if(koef == 1)

                ordMth = Integer.parseInt(val);

            else

                ordYear = Integer.parseInt(val);

            koef++;

        }
        koef = 0;

        for(String val: date.split("."))
        {
            if(koef == 0)
                day = Integer.parseInt(val);
            else if(koef == 1)
                mth = Integer.parseInt(val);
            else
                yr = Integer.parseInt(val);
            koef++;
        }
        return day == ordDay && mth == ordMth && yr == ordYear;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Order)) return false;
        Order order = (Order) obj;
        return ordId == order.ordId && clId == order.clId && ordPrice == order.ordPrice && ordDate.equals(order.ordDate);
    }
    @Override
    public int hashCode() {
        return Objects.hash(ordId, clId, ordPrice, ordDate);
    }
    public boolean checkClient(int id){
        return clId == id;
    }
    @Override
    public String toString(){
        return "[Order] ClientId: " + clId + ", OrderId: " + ordId + ", OrderPrice: " + ordPrice;
    }





}
