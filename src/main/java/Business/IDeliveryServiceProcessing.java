package Business;

import Data.UserLevel;

import java.io.IOException;
import java.util.NoSuchElementException;

public interface IDeliveryServiceProcessing {
    /**
     * @param baseProduct Base product to be added
     * @pre !insertedItems.contains(baseProduct)
     * @post none
     */
    void addBaseProduct(BaseProduct baseProduct) throws IllegalArgumentException;

    /**
     * @param baseProduct Base product to be added
     * @pre !temporaryItems.contains(baseProduct)
     * @post none
     */
    void updateNewCompositeProduct(BaseProduct baseProduct) throws IllegalArgumentException;

    /**
     * @param name Name of the new composite product
     * @pre !temporaryItems.isEmpty() && !name.isEmpty() && !insertedItems.contains(product)
     * @post none
     */
    void insertCompositeProduct(String name);

    /**
     * @param username Username of the account
     * @param password Password of the account
     * @param role User role
     * @throws Exception User already in list
     * @pre !username.isEmpty() && !password.isEmpty()
     * @post none
     */
    void addUser(String username, String password, String role) throws Exception;

    /**
     * @param username Username of the account
     * @param password Password of the account
     * @return UserLevel
     * @throws NoSuchElementException Element not found in the user list
     * @pre !username.isEmpty && !password.isEmpty()
     * @post none
     */
    UserLevel checkUser(String username, String password) throws NoSuchElementException;

    /**

     * @param product Product to be added to the order
     * @pre none
     * @post none
     */
    void addItem(BaseProduct product);

    /**
     * @param product Product to be added to the order
     * @pre none
     * @post none
     */
    void addItem(CompositeProduct product);

    /**
     * @param name Name of the product to be removed
     * @pre !name.isEmpty()
     * @post found == true
     */
    void deleteItemOrder(String name);

    /**
     * @param name Name of the product to be removed
     * @pre !name.isEmpty()
     * @post found == true
     */
    void deleteItem(String name);

    /**
     * @param name Name of the product
     * @param rating The new rating value
     * @param calories The new calories value
     * @param protein The new protein value
     * @param fat The new fat value
     * @param sodium The new sodium value
     * @param price The new price value
     * @throws NumberFormatException One of the values isn't a number
     * @pre !name.isEmpty()
     * @post found == true
     */
    void modifyItem(String name, String rating, String calories, String protein, String fat, String sodium, String price) throws NumberFormatException;

    /**
     * @param username The user who made the order
     * @throws IOException The bill can't be created
     * @pre !ordersList.isEmpty() && clientId >= 0
     * @post none
     */
    void addOrder(String username) throws IllegalArgumentException, IOException;

    /**
     * @param order The object associated with the order
     * @pre none
     * @post none
     */
    void addOrder(Order order);

    /**
     * @param name Product that contains that name
     * @param r_min Minimum rating of the product
     * @param r_max Maximum rating of the product
     * @param p_min Minimum protein of the product
     * @param p_max Maximum protein of the product
     * @param c_min Minimum calories of the product
     * @param c_max Maximum calories of the product
     * @param f_min Minimum fat of the product
     * @param f_max Maximum fat of the product
     * @param s_min Minimum sodium of the product
     * @param s_max Maximum sodium of the product
     * @param pr_min Minimum price of the product
     * @param pr_max Maximum price of the product
     * @throws NumberFormatException Values are not numbers
     * @pre none
     * @post !searchedItems.isEmpty()
     */
    void filterItems(String name, String r_min, String r_max, String p_min, String p_max, String c_min, String c_max, String f_min, String f_max, String s_min, String s_max, String pr_min, String pr_max) throws NumberFormatException;

    /**
     * @param hour_before The before hour for the report
     * @param hour_after The after hour for the report
     * @throws IOException Can't create the file where to save the report
     */

    void generateReport1(String hour_before, String hour_after) throws IOException;

    /**
     * @param counter Minimum number of times that the product must appear
     * @throws IOException Can't create the file to write the report 2
     * @pre counter > 0
     * @post none
     */

    void generateReport2(long counter) throws IOException;

    /**
     * @param counter Minimum number of times the client made an order
     * @param price Minimum price of the order
     * @throws IOException Can't create the file for the report
     * @pre counter > 0 && price > 0
     * @post none
     */

    void generateReport3(long counter, int price) throws IOException;

    /**
     * @param date The date used for the report
     * @throws IOException Can't create the file for the report
     * @pre none
     * @post none
     */
}
