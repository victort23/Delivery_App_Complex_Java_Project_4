package Business;
import java.util.ArrayList;


import java.io.Serializable;
public class CompositeProduct extends MenuItem implements Serializable {

    /////////////////DONE
    private ArrayList<MenuItem> itms;
    //Produsul compus constructor
    public CompositeProduct(String name, ArrayList<MenuItem> items){
        super(name, 0, 0, 0, 0, 0, 0);
        double rating = 0;
        int proteine = 0,calorii=0,grasime=0,sodiu=0,pret=0;
        this.itms = items;



        for(MenuItem i: items)
        {
            rating += i.getRating();    grasime += i.getFat();
            proteine += i.getProtein();            sodiu += i.getSodium();
            calorii += i.getCalories();      pret += i.getPrice();


        }
        rating /= items.size();
        this.setRating(rating);       this.setCalories(calorii);
        this.setFat( grasime);   this.setSodium(sodiu);
        this.setPrice(pret);        this.setProtein(proteine);
    }
    //Obtinem itemele
    public ArrayList<MenuItem> getItems(){
        return itms;
    }
    //Creeam pretul
    @Override
    public int computePrice(){
        return getPrice();
    }
    @Override
    //Suprascriem Equals
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof CompositeProduct)) return false;
        CompositeProduct compositeProduct = (CompositeProduct) obj;
        if(super.equals(compositeProduct)){
            for(MenuItem item: compositeProduct.itms){
                if(!this.itms.contains(item)){
                    return false;
                }
            }
        }
        else
            return false;
        return true;
    }


}
