package Business;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {
    //////////////////////DONE
    public BaseProduct(String nume, double rating, int calorii, int proteine, int grasime, int sodiu, int pret) {
        super(nume, rating, calorii, proteine, grasime, sodiu, pret);
    }

    @Override
    //Suprascriem Equals
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof BaseProduct)) return false;
        BaseProduct baseProduct = (BaseProduct) obj;
        return this.getName().equals(baseProduct.getName());
    }

    @Override
    //Suprascriem compunerea pretului
    public int computePrice(){
        return getPrice();
    }
}
