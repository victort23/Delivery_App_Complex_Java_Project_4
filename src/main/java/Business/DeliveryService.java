package Business;

import Data.Serializator;
import Data.UserLevel;
import Data.UsersList;
import Data.User;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.*;
import java.util.List;

//////////////////DONE
public class DeliveryService implements IDeliveryServiceProcessing, Serializable {
    private List<MenuItem> allItems;
    private ArrayList<MenuItem> insItems;
    private ArrayList<BaseProduct> tempItems;
    private UsersList userii;
    private HashMap<Order, ArrayList<MenuItem>> ordMap;
    private ArrayList<MenuItem> ordersList;
    private ArrayList<MenuItem> srchItems;
    private transient PropertyChangeSupport support;
    private Order lastOrd;
    public int orderId=0;
    public ArrayList<MenuItem> aux;
    protected boolean isWellFormed(){
        assert !userii.isEmpty() : "Users are missing";
        assert userii.verifExistaAdmin(): "Adminu nu e inregistrat";
        assert !allItems.isEmpty(): "Lipseste lista de produse";
        return true;
    }
    private DeliveryService() throws IOException {
        allItems = new ArrayList<>();   allItems = importProducts();

        insItems = new ArrayList<>(); tempItems = new ArrayList<>();

        ordMap = new HashMap<>();   userii = new UsersList();

        lastOrd = null;  support = new PropertyChangeSupport(this);
        srchItems = new ArrayList<>();       ordersList = new ArrayList<>();

        isWellFormed();
    }

    private static DeliveryService getSerialization(){
        File file = new File("delivery.ser");
        if(file.exists()){
            return Serializator.deserialize("delivery.ser");
        }
        return null;
    }
    //Obtinem comanda
    public static DeliveryService getDelivery() throws IOException {
        DeliveryService d;
        d = getSerialization();
        if(d == null)
            return new DeliveryService();
        else
        {
            d.isWellFormed();   d.setSupport();
            return d;
        }
    }
    //Setam supp
    public void setSupport(){
        support = new PropertyChangeSupport(this);
    }
    //Obt all items
    public List<MenuItem> getAllItems(){
        return allItems;
    }
    //Obtinem itemele inserate
    //
    //
    //
    //
    public ArrayList<MenuItem> getInsertedItems(){
        return insItems;
    }


    public void addBaseProduct(BaseProduct baseProduct)
    {
        assert !insItems.contains(baseProduct): "Product already exists in the data base";
        insItems.add(baseProduct);
    }

    public void updateNewCompositeProduct(BaseProduct baseProduct)
    {
        assert !tempItems.contains(baseProduct): "Product already exists in the data base";
        tempItems.add(baseProduct);
    }
    //Inseram produs compus
    public void insertCompositeProduct(String name) {
        assert !tempItems.isEmpty() : "Produsul compus e gol";
        assert !name.isEmpty() :"Produsul compus trebuie sa aiba nume";
        ArrayList<MenuItem> prods = new ArrayList<>(tempItems);
        CompositeProduct prod = new CompositeProduct(name, prods);
        assert !insItems.contains(prod) : "Produsul deja exista in Baza de Date";
        tempItems.clear();insItems.add(prod);

    }
    //Importam produsele din fisierul .csv
    private List<MenuItem> importProducts()
    {
        Pattern pattern = Pattern.compile(",");
        try (Stream<String> lines = Files.lines(Path.of("products.csv"))){
            allItems = lines.skip(1).map(line->{
                String[] att = pattern.split(line);
                return new BaseProduct(
                        att[0],
                        Double.parseDouble(att[1]),
                        Integer.parseInt(att[2]),
                        Integer.parseInt(att[3]),
                        Integer.parseInt(att[4]),
                        Integer.parseInt(att[5]),
                        Integer.parseInt(att[6]));
            }).distinct().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allItems;
    }
    //Adaugam user nou in functie de username password si rol
    public void addUser(String user, String pass, String role) throws Exception {
        assert !user.isEmpty() : "Lipseste username";  assert !pass.isEmpty() : "Lipseste parola";
        userii.addUser(user, pass, role);
    }

    @Override
    public UserLevel checkUser(String username, String password) throws NoSuchElementException {
        return null;
    }


    //Aici facem assert in caz ca lipsesc datele din campurile de logare
    public UserLevel verifUser(String user, String pass) throws NoSuchElementException
    {
        assert !user.isEmpty() && !pass.isEmpty() : "Useru sau parola lipseste";
        return userii.verifUser(user, pass);
    }
    //Aici adaugam item de baza in lista de comenzi
    public void addItem(BaseProduct prod){
        ordersList.add(prod);
    }

    @Override
    public void addItem(CompositeProduct product) {

    }

    @Override
    public void deleteItemOrder(String name) {

    }

    //Stergem itemul dupa nume daca il gasim din order list
    public void stergeItemOrder(String nume)
    {
        assert !nume.isEmpty() : "Lipseste numele produs"; boolean gasit = false;

        for(MenuItem item: ordersList)
        {
            if(item.getName().equals(nume))
            {
                ordersList.remove(item); gasit = true;
                break;
            }
        }
        assert gasit : "Produsu nu se afla in comenzi";
    }
    //Stergem item
    public void deleteItem(String nume)
    {
        assert !nume.isEmpty() :"Lipseste numele";    boolean gasit= false;

        for(MenuItem item: insItems)
        {
            if(item.getName().equals(nume))
            {
                insItems.remove(item); gasit = true;
                break;
            }
        }
        assert gasit : "Nu exista itemu";
    }

    public void modifyItem(String nume, String rating, String calorii, String proteine, String grasimi, String sodiu, String pret) throws NumberFormatException
    {
        assert !nume.isEmpty() : "Lipseste numele";boolean gasit = false;
        for(MenuItem item: insItems)
        {
            if(item.getName().equals(nume))
            {
                if(!rating.isEmpty()) item.setRating(Double.parseDouble(rating));

                if(!pret.isEmpty())item.setPrice(Integer.parseInt(pret));
                if(!proteine.isEmpty())item.setProtein(Integer.parseInt(proteine));

                if(!calorii.isEmpty())item.setCalories(Integer.parseInt(calorii));
                if(!grasimi.isEmpty())item.setFat(Integer.parseInt(grasimi));
                if(!sodiu.isEmpty())item.setSodium(Integer.parseInt(sodiu));
                gasit = true;
                break;
            }
        }
        assert gasit : "Nu exista item-ul";
    }

    public void addOrder(String usname) throws IllegalArgumentException, IOException
    {
        assert !ordersList.isEmpty() : "Lista de comenzi e goala!";
        int clId = userii.getUserId(usname),pret=0;

        assert clId >= 0 : "Nu exista username-u";

        Set<Order> ords = ordMap.keySet();orderId = ords.size() + 1;


        for (MenuItem item : ordersList) {
            pret += item.computePrice();
        }
        Date date = new Date();  Order order = new Order(orderId, clId, date, pret);
        aux = new ArrayList<>(ordersList);ordMap.put(order, aux);
        ordersList.clear();  setOrder(order);
        order.printBill(usname, aux);
    }
    //Raportul 1 cu intervalul orar
    public void generateReport1(String hb, String ha) throws IOException {
        ArrayList<Order> orders = new ArrayList<>(ordMap.keySet());
        ArrayList<Order> list = orders.stream().filter(order -> order.checkDate(hb,ha)).collect(Collectors.toCollection(ArrayList::new));
        String fN = "Report1.txt";
        SimpleDateFormat form = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");fN += form.format(new Date()) ;

        File file = new File(fN);  file.createNewFile();


        FileWriter fw = new FileWriter(file, true);
        fw.append("Report 1 at " + form.format(new Date()) + "\n");
        for(Order o: list)
        {
            fw.append(o.toString() + "\n");
        }
        fw.close();
    }
    //Raportul 2 cu counter
    public void generateReport2(long cnt) throws IOException
    {
        assert cnt > 0 : "Counter value must be positive";
        long[] count = new long[insItems.size()];
        ArrayList<Order> orders = new ArrayList<>(ordMap.keySet());
        for(Order order: orders)
        {
            for(MenuItem item:insItems)
            {
                long num = ordMap.get(order).stream().filter(menuItem -> menuItem.equals(item)).count();
                count[insItems.indexOf(item)] += num;
            }
        }

        String fName = "Raport2.txt";
        SimpleDateFormat form = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss"); fName += form.format(new Date()) ;
        File file = new File(fName); file.createNewFile();
        FileWriter fw = new FileWriter(file, true);
        fw.append("Report 2 at " + form.format(new Date()) + "\n");
        for(int i = 0; i < insItems.size(); i++){
            if(count[i] >cnt)
                fw.append(insItems.get(i).toString() + "\n");
        }
        fw.close();
    }
    //Raportul 3 cu counter si pret
    public void generateReport3(long cnt, int pret) throws IOException {
        assert cnt > 0 : "Cnt has to be positive";
        assert pret > 0 : "Pret value has to be positive";
        int koef = 0;
        ArrayList<Order> orders= new ArrayList<>(ordMap.keySet());
        ArrayList<User> clients = userii.getClients();
        long[] count = new long[clients.size()];

        for(User u: clients){
            int clientId = userii.getUserId(u.getUsername());
            count[koef] = orders.stream().filter(order -> order.checkClient(clientId) && order.getOrderPrice() >= pret).count();
            koef++;
        }
        String fName = "Raport3.txt";
        SimpleDateFormat form = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        fName += form.format(new Date()) ;
        File file = new File(fName);     file.createNewFile();
        FileWriter fw = new FileWriter(file, true);   fw.append("Report 3 at " + form.format(new Date()) + "\n");
        koef = 0;
        for(User u: clients){
            if(count[koef] >= cnt){
                fw.append(u.toString() + "\n");
            }
            koef++;
        }
        fw.close();
    }


    //Le afisam
    @Override
    public String toString()
    {
        String rez = "Itemele inserate:\n";

        for(MenuItem i: insItems)
            rez = rez + i.toString() + "\n";

        return rez;
    }
    public void addOrder(Order order){
        ordMap.put(order, insItems);      setOrder(order);
    }
    //Obtinem map ul pentru comenzi
    public HashMap<Order, ArrayList<MenuItem>> getOrdersMap(){
        return ordMap;
    }
//Setam o comanda

    public void setOrder(Order ord)
    {
        support.firePropertyChange("order", this.lastOrd, ord);
        lastOrd = ord;
    }
    //Filtram itemele
    public void filterItems(String name, String r_min, String r_max, String p_min, String p_max, String c_min, String c_max, String f_min, String f_max, String s_min, String s_max, String pr_min, String pr_max) throws NumberFormatException
    {
        srchItems = insItems.stream().filter(menuItem -> menuItem.checkItem(name, r_min, r_max, p_min, p_max, c_min, c_max, f_min, f_max, s_min, s_max, pr_min, pr_max)).collect(Collectors.toCollection(ArrayList::new));  assert !srchItems.isEmpty() : "Nici un item nu respecta criteriul";

    }
    //Obtinem itemele cautate
    public ArrayList<MenuItem> getSearchedItems(){
        return srchItems;
    }
    //Obtinem lista de comenzi
    public ArrayList<MenuItem> getOrdersList(){
        return ordersList;
    }
    //ppl
    public void addPropertyChangeListener(PropertyChangeListener pcl){
        support.addPropertyChangeListener(pcl);
    }
    //rpl
    public void removePropertyChangeListener(PropertyChangeListener pcl){
        support.removePropertyChangeListener(pcl);
    }


}
