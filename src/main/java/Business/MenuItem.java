package Business;

import java.io.Serializable;
import java.util.ArrayList;
///////////////////////DONE
public abstract class MenuItem implements Serializable {
    private String nume;
    private double rating;
    private int calorii,
            proteine,
            grasime,
            sodiu,
            pret;

    public MenuItem(String name, double rating, int calories, int protein, int fat, int sodium, int price) {
        this.nume = name;
        this.rating = rating;        this.calorii = calories;
        this.proteine = protein;        this.grasime = fat;
        this.sodiu = sodium;        this.pret = price;

    }

    public String getName()
    {
        return nume;
    }


    //Getters and setters pentru atributele itemului
    public double getRating()
    {
        return rating;
    }

    public void setRating(double rating)
    {
        this.rating = rating;
    }

    public void setCalories(int calories)
    {
        this.calorii = calories;
    }

    public int getProtein()
    {
        return proteine;
    }

    public void setProtein(int protein)
    {
        this.proteine = protein;
    }

    public int getFat()
    {
        return grasime;
    }
    public void setName(String name)
    {
        this.nume = name;
    }

    public int getCalories()
    {
        return calorii;
    }
    public void setFat(int fat)
    {
        this.grasime = fat;
    }

    public int getSodium()
    {
        return sodiu;
    }

    public void setSodium(int sodium)
    {
        this.sodiu = sodium;
    }

    public int getPrice()
    {
        return pret;
    }

    public void setPrice(int price)
    {
        this.pret = price;
    }
    //Verificam itemul daca se afla in intervalele corespunzatoare atributelor
    public boolean checkItem(String nume, String r_min, String r_max, String p_min, String p_max, String c_min, String c_max, String f_min, String f_max, String s_min, String s_max, String pr_min, String pr_max) throws NumberFormatException{
        double rating_min = !r_min.isEmpty() ? Double.parseDouble(r_min) : Double.NEGATIVE_INFINITY;
        double rating_max = !r_max.isEmpty() ? Double.parseDouble(r_max) : Double.POSITIVE_INFINITY;
        int protein_min = !p_min.isEmpty() ? Integer.parseInt(p_min) : Integer.MIN_VALUE,
                proteine_max = !p_max.isEmpty() ? Integer.parseInt(p_max) : Integer.MAX_VALUE,
                calorii_min = !c_min.isEmpty() ? Integer.parseInt(c_min) : Integer.MIN_VALUE,
                calorii_max = !c_max.isEmpty() ? Integer.parseInt(c_max) : Integer.MAX_VALUE,
                grasime_min = !f_min.isEmpty() ? Integer.parseInt(f_min) : Integer.MIN_VALUE,
                grasime_max = !f_max.isEmpty() ? Integer.parseInt(f_max) : Integer.MAX_VALUE,
                sodiu_min = !s_min.isEmpty() ? Integer.parseInt(s_min) : Integer.MIN_VALUE,
                sodiu_max = !s_max.isEmpty() ? Integer.parseInt(s_max) : Integer.MAX_VALUE,
                pret_min = !pr_min.isEmpty() ? Integer.parseInt(pr_min) : Integer.MIN_VALUE,
                pret_max = !pr_max.isEmpty() ? Integer.parseInt(pr_max) : Integer.MAX_VALUE;

        if(this.nume.contains(nume) &&
                rating_min <= rating && rating <= rating_max &&
                protein_min <= proteine && proteine <= proteine_max &&
                calorii_min <= calorii && calorii <= calorii_max &&
                grasime_min <= grasime && grasime <= grasime_max &&
                sodiu_min <= sodiu && sodiu <= sodiu_max &&
                pret_min <= pret && pret <= pret_max)
            return true;
        return false;
    }
    //Metoda toString pentru afisare
    @Override
    public String toString()
    {
        String res = "";
        if(this instanceof BaseProduct)
            res += "[BASE_PRODUCT] ";
        else
            res += "[COMPOSITE_PRODUCT] ";
        res = res + "Nume: " + nume + ", rating: " + rating + "/5 " + ", price: " + pret + "\n";
        res = res + "Calorii: " + calorii + ", proteine: " + proteine;
        if(this instanceof CompositeProduct){
            res += "\nProdusul contine:\n";
            ArrayList<MenuItem> lista = ((CompositeProduct)this).getItems();
            for(MenuItem item: lista){
                res += item.toString(); res += "\n";

            }
        }
        return res;
    }
    //Suprascriem si aici metoda Equals
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof MenuItem)) return false;
        MenuItem item = (MenuItem) o;
        return Double.compare(item.rating, rating) == 0 && calorii == item.calorii && proteine == item.proteine && grasime == item.grasime && sodiu == item.sodiu && pret == item.pret && nume.equals(item.nume);
    }

    public abstract int computePrice();
}
