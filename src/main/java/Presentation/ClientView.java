package Presentation;

import Business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
/////////////////////////////////DONE
public class ClientView extends JFrame {
    private final String username;

    private final JTextField nume_t = new JTextField(20);
    private final JTextField maxCalorii_t = new JTextField(5);    private final JTextField maxGrasime_t = new JTextField(5);

    private final JTextField minSodiu_t = new JTextField(5);    private final JTextField maxSodiu_t = new JTextField(5);

    private final JTextField minRating_t = new JTextField(5);    private final JTextField maxPret_t = new JTextField(5);
    private final JTextField maxRating_t = new JTextField(5);    private final JTextField minCaloriis_t = new JTextField(5);


    private final JTextField minPret_t = new JTextField(5);    private final JTextField minProteine_t = new JTextField(5);

    private final JTextField minGrasime_t = new JTextField(5);    private final JTextField maxProteine_t = new JTextField(5);





    private JTable searchedItems_tb;

    private final JButton search_b = new JButton("Cauta produse");
    private final JButton addItem_b = new JButton("Adauga item la comanda");
    private final JButton delItem_b = new JButton("Sterge item din comanda");
    private final JButton pay_b= new JButton("Plateste");
    private final JTextArea printArea = new JTextArea(20,30);

    private JFrame frameCl;

    public String getUsername(){
        return username;
    }

    public ClientView(String username){
        this.username = username;

        JPanel filterName = new JPanel();
        filterName.setLayout(new FlowLayout());
        filterName.add(new JLabel("Nume contains: "));
        filterName.add(nume_t);

        JPanel ratingFilter = new JPanel();
        ratingFilter.setLayout(new FlowLayout());
        ratingFilter.add(new JLabel("Rating between: "));
        ratingFilter.add(minRating_t);
        ratingFilter.add(new JLabel(" and "));
        ratingFilter.add(maxRating_t);

        JPanel caloriesFilter = new JPanel();
        caloriesFilter.setLayout(new FlowLayout());
        caloriesFilter.add(new JLabel("Calorii between: "));
        caloriesFilter.add(minCaloriis_t);
        caloriesFilter.add(new JLabel(" and "));
        caloriesFilter.add(maxCalorii_t);

        JPanel proteinsFilter = new JPanel();
        proteinsFilter.setLayout(new FlowLayout());
        proteinsFilter.add(new JLabel("Proteine between: "));
        proteinsFilter.add(minProteine_t);
        proteinsFilter.add(new JLabel(" and "));
        proteinsFilter.add(maxProteine_t);

        JPanel fatFilter = new JPanel();
        fatFilter.setLayout(new FlowLayout());
        fatFilter.add(new JLabel("Grasime between: "));
        fatFilter.add(minGrasime_t);
        fatFilter.add(new JLabel(" and "));
        fatFilter.add(maxGrasime_t);

        JPanel sodiumFilter = new JPanel();
        sodiumFilter.setLayout(new FlowLayout());
        sodiumFilter.add(new JLabel("Sodiu between: "));
        sodiumFilter.add(minSodiu_t);
        sodiumFilter.add(new JLabel(" and "));
        sodiumFilter.add(maxSodiu_t);

        JPanel priceFilter = new JPanel();
        priceFilter.setLayout(new FlowLayout());
        priceFilter.add(new JLabel("Pret between: "));
        priceFilter.add(minPret_t);
        priceFilter.add(new JLabel(" and "));
        priceFilter.add(maxPret_t);

        JPanel buttonSet1 = new JPanel();
        buttonSet1.setLayout(new FlowLayout());
        buttonSet1.add(addItem_b);
        buttonSet1.add(delItem_b);

        JPanel buttonSet2 = new JPanel();
        buttonSet2.setLayout(new FlowLayout());
        buttonSet2.add(search_b);
        buttonSet2.add(pay_b);

        JPanel orderSummary = new JPanel();
        orderSummary.setLayout(new FlowLayout());
        orderSummary.add(printArea);

        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        content.add(filterName);
        content.add(ratingFilter);
        content.add(caloriesFilter);
        content.add(proteinsFilter);
        content.add(fatFilter);
        content.add(sodiumFilter);
        content.add(priceFilter);
        content.add(buttonSet1);
        content.add(buttonSet2);
        content.add(orderSummary);

        WindowListener listener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(frameCl != null)
                    frameCl.dispose();
            }
        };
        this.addWindowListener(listener);

        this.setContentPane(content);
        this.pack();
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Client View");
    }

    public void createTable(ArrayList<MenuItem> items){
        int koef = 0;
        String[] columnNames ={
                "Nume",
                "Rating",
                "Calorii",
                "Proteine",
                "Grasime",
                "Sodiu",
                "Pret"};
        Object[][] data = new Object[items.size()][7];

        for(Business.MenuItem item: items){
            data[koef][0] = item.getName();
            data[koef][1] = item.getRating();
            data[koef][2] = item.getCalories();
            data[koef][3] = item.getProtein();
            data[koef][4] = item.getFat();
            data[koef][5] = item.getSodium();
            data[koef][6] = item.getPrice();
            koef++;
        }

        searchedItems_tb = new JTable(data, columnNames);
        JScrollPane scrollPane = new JScrollPane(searchedItems_tb);
        searchedItems_tb.setFillsViewportHeight(true);

        frameCl = new JFrame();
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(scrollPane);

        frameCl.setContentPane(panel);
        frameCl.pack();
        frameCl.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frameCl.setTitle("Menu - Client View");
        frameCl.setVisible(true);
    }
    //Update table
    public void updateTable(ArrayList<MenuItem> list)
    {
        disposeTable();
        createTable(list);
    }
    //Disppse Table
    public void disposeTable()
    {
        frameCl.dispose();
    }
    //Adaugam Item listener
    public void addItemButtonListener(ActionListener a)
    {
        addItem_b.addActionListener(a);
    }
    //Delete item listerner
    public void deleteItemButtonListener(ActionListener a)
    {
        delItem_b.addActionListener(a);
    }
    //Search listeneer
    public void addSearchButtonListener(ActionListener a)
    {
        search_b.addActionListener(a);
    }

    //Obtinem nume listener
    public String getNameText()
    {
        return nume_t.getText();
    }

    //Pay button listener
    public void payButtonListener(ActionListener a)
    {
        pay_b.addActionListener(a);
    }

    //getMaxCal listener
    public String getMaxCaloriesText()
    {
        return maxCalorii_t.getText();
    }
    //get Min Rating listener
    public String getMinRatingText()
    {
        return minRating_t.getText();
    }
    //Get MAx raing listener
    public String getMaxRatingText()
    {
        return maxRating_t.getText();
    }
    public String getMinProteinsText()
    {
        return minProteine_t.getText();
    }
    public String getMinCaloriesText()
    {
        return minCaloriis_t.getText();
    }


    public String getMaxProteinsText()
    {
        return maxProteine_t.getText();
    }
    public String getMinSodiumText()
    {
        return minSodiu_t.getText();
    }
    public String getMinFatText()
    {
        return minGrasime_t.getText();
    }
    //Steam text area
    public void setTextArea(ArrayList<MenuItem> list)
    {
        printArea.setText("");
        for(MenuItem item: list){
            printArea.append(item.getName() + "\n");
        }
    }

    public String getMaxFatText()
    {
        return maxGrasime_t.getText();
    }
    public String getMaxPriceText()
    {
        return maxPret_t.getText();
    }


    public String getMaxSodiumText()
    {
        return maxSodiu_t.getText();
    }
    public int getIndexOfSelectedRow()
    {
        return searchedItems_tb.getSelectedRow();
    }

    public String getMinPriceText()
    {
        return minPret_t.getText();
    }




}
