package Presentation;

import Business.Order;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.event.ActionListener;

//////////////////////////////DONE
public class EmployeeView extends JFrame implements PropertyChangeListener {
    private String username;

    private  JTextField ordId_t;    private  JTextField mess_t;
    private Order ord;    public JButton Done=new JButton("Prepare");
    public String getUsername(){
        return username;
    }
    //Constructor pt fereastra de employeer
    public EmployeeView(String username)
    {
        this.username = username;
        ordId_t = new JTextField(5);     mess_t = new JTextField(100);

        ordId_t.setEditable(false);        mess_t.setEditable(false);

        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());

        JPanel messages = new JPanel();
        messages.setLayout(new BoxLayout(messages, BoxLayout.PAGE_AXIS));
        messages.add(new JLabel("Comanda ID: "));  messages.add(new JLabel("Mesajul: "));

        JPanel textFields = new JPanel();
        textFields.setLayout(new BoxLayout(textFields, BoxLayout.PAGE_AXIS));
        textFields.add(ordId_t);      textFields.add(mess_t);


        JPanel butonProcesare= new JPanel();    butonProcesare.add(Done);
        content.add(messages);        content.add(textFields);
        content.add(butonProcesare);

        this.setContentPane(content);        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.pack();        this.setTitle("Employee View");

    }
    public void propertyChange(PropertyChangeEvent evt)
    {
        this.setOrder((Order)evt.getNewValue());
        setOrderId_tf(String.valueOf(ord.getOrderId()));
        setMessage_tf("Clientul cu id-ul " + ord.getClientId() + " i se proceseaza comanda cu pretul " + ord.getOrderPrice());
    }
    public void setOrderId_tf(String val){
        ordId_t.setText(val);
    }

    public void setMessage_tf(String val){
        mess_t.setText(val);
    }
    public void prepareButtonListener(ActionListener a){
        Done.addActionListener(a);
    }
    public Order getOrder(){
        return ord;
    }

    private void setOrder(Order newOrder){
        ord = newOrder;
    }



}
