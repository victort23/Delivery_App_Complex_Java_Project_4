package Presentation;

import Business.*;
import Data.UserLevel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller{
    private LogInView logView;
    private ClientView clView;
    private EmployeeView empView;
    private AdministratorView adminView;
    private DeliveryService delivery;

    public Controller(LogInView logInView, DeliveryService d)
    {
        logView = logInView;
        delivery = d;

        logInView.addLogInListener(new LogInListener());
        logInView.addSignInListener(new SignInListener());
        logInView.addSignUpListener(new SignUpListener());
    }




    private void setEmployeeListener()
    {
        empView.prepareButtonListener(new PrepareListener());
    }
    private void setClientListener(){
        clView.addSearchButtonListener(new SearchListener());

        clView.payButtonListener(new PayListener());

        clView.deleteItemButtonListener(new DeleteListener());

        clView.addItemButtonListener(new AddItemListener());

    }
    private class PrepareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            empView.setOrderId_tf(String.valueOf(""));
            delivery.aux=null;  empView.setMessage_tf(" ");

        }
    }


    private class DeleteListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = clView.getNameText();
            try {
                delivery.deleteItemOrder(name);
                clView.setTextArea(delivery.getOrdersList());
            } catch (AssertionError error){
                logView.showError(error.getMessage());
            }
        }
    }

    //Listener processare comanda
    private class PayListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String username = clView.getUsername();
            try {
                delivery.addOrder(username);     empView=new EmployeeView(username);
                clView.setTextArea(delivery.getOrdersList());
                System.out.println(delivery.getOrdersList().size());      logView.showError("Order processed!");

            } catch (IOException  |  AssertionError exception) {
                logView.showError(exception.getMessage());
            }
        }
    }

    //Adaudgare din camp uri listener
    private class AddToCompositeFieldsListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = adminView.getBaseText();
            String rating = adminView.getRatingText();
            String calories = adminView.getCaloriesTest();
            String protein = adminView.getProteinText();
            String fat = adminView.getFatText();
            String sodium = adminView.getSodiumText();
            String price = adminView.getPriceText();
            if (name.isEmpty() || rating.isEmpty() || calories.isEmpty() || protein.isEmpty() || fat.isEmpty() || sodium.isEmpty() || price.isEmpty()){
                logView.showError("All fields must have a value!");
            }
            else {
                try{
                    BaseProduct product = new BaseProduct(name, Double.parseDouble(rating), Integer.parseInt(calories), Integer.parseInt(protein), Integer.parseInt(fat), Integer.parseInt(sodium), Integer.parseInt(price));
                    delivery.updateNewCompositeProduct(product);
                    adminView.showMessage("Item added successfully in the composite product");
                } catch(NumberFormatException | AssertionError ex){
                    logView.showError(ex.getMessage());
                }
            }
        }
    }
    //Stegere item liestener pt buton
    private class DeleteItemListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = adminView.getBaseText();
            if(!name.isEmpty()){
                try {
                    delivery.deleteItem(name);
                    adminView.showMessage("Item deleted successfully from the menu");
                } catch(IllegalArgumentException | AssertionError ex) {
                    logView.showError(ex.getMessage());
                }
            } else {
                logView.showError("Field name must not be empty");
            }
        }
    }
    //Aduagare item listener
    private class AddItemListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = clView.getIndexOfSelectedRow();
            MenuItem product = delivery.getSearchedItems().get(id);
            if(product instanceof BaseProduct){
                delivery.addItem((BaseProduct) product);
            }
            else
                delivery.addItem((CompositeProduct) product);
            clView.setTextArea(delivery.getOrdersList());

        }
    }

    //Modificare item listener pt buton
    private class ModifyItemListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nume = adminView.getBaseText(),
                    rating = adminView.getRatingText(),calorii = adminView.getCaloriesTest(), proteine = adminView.getProteinText(), grasime = adminView.getFatText(),
                    sodiu = adminView.getSodiumText(),
                    pret = adminView.getPriceText();
            try{
                delivery.modifyItem(nume, rating, calorii, proteine, grasime, sodiu, pret);
                adminView.showMessage("Item modified successfully in the menu");
            } catch(IllegalArgumentException | AssertionError ex){
                logView.showError(ex.getMessage());
            }
        }
    }
    private class AddBaseFromFieldsListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = adminView.getBaseText();
            String rating = adminView.getRatingText();
            String calories = adminView.getCaloriesTest();
            String protein = adminView.getProteinText();
            String fat = adminView.getFatText();
            String sodium = adminView.getSodiumText();
            String price = adminView.getPriceText();
            if (name.isEmpty() || rating.isEmpty() || calories.isEmpty() || protein.isEmpty() || fat.isEmpty() || sodium.isEmpty() || price.isEmpty()){
                logView.showError("All fields must have a value!");
            }
            else {
                try{
                    BaseProduct product = new BaseProduct(name, Double.parseDouble(rating), Integer.parseInt(calories), Integer.parseInt(protein), Integer.parseInt(fat), Integer.parseInt(sodium), Integer.parseInt(price));
                    delivery.addBaseProduct(product);
                    adminView.showMessage("Item added successfully in the menu");
                } catch(NumberFormatException | AssertionError ex){
                    logView.showError(ex.getMessage());
                }
            }
        }
    }

    //Adaugare produs de baza listeneer
    private class AddBaseFromTableListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            int r = adminView.getIndexSelectedRow();
            if(r == -1)
                logView.showError("Nu s-a selectat nici un rand");
            else {
                BaseProduct product = (BaseProduct) delivery.getAllItems().get(r);
                try {
                    delivery.addBaseProduct(product);
                } catch ( AssertionError exception){
                    logView.showError(exception.getMessage());
                }
            }
        }
    }
    //Adaugare composite listener pt buton
    private class AddToCompositeTableListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            int r = adminView.getIndexSelectedRow();
            if(r == -1)
                logView.showError("There is no selected row");
            else {
                BaseProduct product = (BaseProduct) delivery.getAllItems().get(r);
                try{
                    delivery.updateNewCompositeProduct(product);
                } catch( AssertionError exception){
                    logView.showError(exception.getMessage());
                }
            }
        }
    }
    //Listener pt raport 1
    private class Report1Listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String bH = adminView.getBeforeHourText(), aH = adminView.getAfterHourText();

            if(! bH.isEmpty() || ! aH.isEmpty()){
                boolean error = false;
                for (char c:  bH.toCharArray()){
                    if(Character.isAlphabetic(c))
                        error = true;
                }
                for(char c:  aH.toCharArray()){
                    if(Character.isAlphabetic(c))
                        error = true;
                }
                if(error){
                    logView.showError("Valorile trebuie sa fie intregi");
                }
                else {
                    try {
                        delivery.generateReport1(bH, aH);
                    } catch(IOException ex){
                        logView.showError(ex.getMessage());
                    }
                }
            }
        }
    }
    //Listener pentru cautare item
    private class SearchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = clView.getNameText();
            String r_min = clView.getMinRatingText();
            String r_max = clView.getMaxRatingText();
            String p_min = clView.getMinProteinsText();
            String p_max = clView.getMaxProteinsText();
            String c_min = clView.getMinCaloriesText();
            String c_max = clView.getMaxCaloriesText();
            String f_min = clView.getMinFatText();
            String f_max = clView.getMaxFatText();
            String s_min = clView.getMinSodiumText();
            String s_max = clView.getMaxSodiumText();
            String pr_max = clView.getMaxPriceText();
            String pr_min = clView.getMinPriceText();
            try {
                delivery.filterItems(name, r_min, r_max, p_min, p_max, c_min, c_max, f_min, f_max, s_min, s_max, pr_min, pr_max);
                clView.updateTable(delivery.getSearchedItems());
            } catch(AssertionError exception){
                logView.showError(exception.getMessage());
            }
        }
    }
    //Adaugare composite item
    private class AddCompositeListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String name = adminView.getCompositeText();
            try{
                delivery.insertCompositeProduct(name); adminView.showMessage("Composite item added successfully in the menu");

            } catch(IllegalArgumentException | AssertionError ex){
                logView.showError(ex.getMessage());
            }
        }
    }

    //Listener pentru report3
    private class Report3Listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                long counter = Integer.parseInt(adminView.getCountText());
                int price = Integer.parseInt(adminView.getPriceReportText());
                delivery.generateReport3(counter, price);
            } catch(NumberFormatException | IOException ex) {
                logView.showError(ex.getMessage());
            }
        }
    }
    private void setAdminListener(){
        adminView.addDeleteItemListener(new DeleteItemListener());
        adminView.addCompositeListener(new AddCompositeListener());

        adminView.addBaseFromTableListener(new AddBaseFromTableListener());       adminView.addRep1Listener(new Report1Listener());

        adminView.addRep2Listener(new Report2Listener());    adminView.addRep3Listener(new Report3Listener());
        adminView.addModifyItemListener(new ModifyItemListener());
        adminView.addToCompositeFieldsListener(new AddToCompositeFieldsListener());     adminView.addToCompositeTableListener(new AddToCompositeTableListener());
        adminView.addBaseFromFieldsListener(new AddBaseFromFieldsListener());
//adminView.addRep4Listener(new Report4Listener());
    }

    //Log in listener
    private class LogInListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            String username = logView.getLogUsername();
            String password = String.valueOf(logView.getLogPassword());
            try{
                UserLevel level = delivery.checkUser(username, password);
                switch (level) {
                    case ADMIN -> {
                        if (adminView != null)
                        {
                            adminView.dispose();            adminView.disposeTable();

                        }
                        adminView = new AdministratorView(username);          setAdminListener();

                        adminView.setVisible(true);       adminView.createTable(delivery.getAllItems());

                    }
                    case CL -> {
                        if (clView != null)
                        {
                            clView.dispose();     clView.disposeTable();

                        }
                        clView = new ClientView(username);             clView.setVisible(true);

                        delivery.filterItems("", "", "", "", "", "", "", "", "", "", "", "", "");
                        clView.createTable(delivery.getSearchedItems());      setClientListener();

                    }
                    case EMPLOYEE -> {
                        if (empView != null)
                            empView.dispose();

                        empView = new EmployeeView(username);
                        setEmployeeListener();
                        if(delivery.orderId!=0 && delivery.aux!=null)
                        {
                            empView.setOrderId_tf(String.valueOf(delivery.orderId));
                            empView.setMessage_tf(delivery.aux.toString());
                        }
                        empView.setVisible(true);
                        delivery.addPropertyChangeListener(empView);
                    }
                }
            } catch(NoSuchElementException | AssertionError exception){
                logView.showError(exception.getMessage());
            }
        }
    }
    //Listener pentru report 2
    private class Report2Listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                long counter = Integer.parseInt(adminView.getQuantityText());
                delivery.generateReport2(counter);
            } catch (NumberFormatException | IOException ex){
                logView.showError(ex.getMessage());
            }
        }
    }
    //Listener Sign up
    private class SignUpListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            logView.createSignUpFrame();
        }
    }
    //Listeneru pentru SIgn in
    private class SignInListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String username = logView.getSignUsername();
            String password = String.valueOf(logView.getSignPassword());
            String role = logView.getSelectedRole().toLowerCase(Locale.ROOT);
            try {
                if (role.equals("client")) {
                    delivery.addUser(username, password, role);
                    logView.showError("The client has been added");
                }
                else{
                    String cod = logView.getCode();
                    if(cod.equals("VICTOR"))
                    {
                        delivery.addUser(username, password, role);
                        logView.showError("Noul utilizator a fost adÄƒugat");
                    }
                    else
                    {
                        logView.showError("Codul introdus nu este corect");
                    }
                }
            } catch (Exception ex)
            {
                logView.showError(ex.getMessage());
            }
        }
    }


}
