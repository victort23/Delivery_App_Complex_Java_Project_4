package Presentation;

import Business.DeliveryService;
import Data.Serializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.awt.event.WindowAdapter;

import java.net.URL;
import java.util.Objects;
/////////////////////DONE
public class LogInView extends JFrame {
    private final String[] roles = {"ADMINISTRATOR", "CLIENT", "EMPLOYEE"};
    private final JTextField usname_t;    private final JComboBox<String> roles_cb;
    private final JTextField newUsname_t;    private final JPasswordField pass_t;

    private final JPasswordField newPass_t;    private final JButton sign_in_b;


    private final DeliveryService ddd;    private final JTextField code_t;

    private final JButton login_b;   private final JButton sign_up_b;


    //Interfata login
    public LogInView(DeliveryService deliveryService) {
        ddd = deliveryService;
        WindowListener listener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("Aplicatia s-a oprit");
                try {
                    Serializator.serialize(ddd);
                } catch (IOException exception) {
                    showError(exception.getMessage());
                }
            }
        };

        this.usname_t = new JTextField(20);     this.code_t = new JTextField(5);
        this.newUsname_t = new JTextField(20);        this.sign_up_b = new JButton("Sign Up");
        this.pass_t = new JPasswordField(20);        this.login_b = new JButton("Log In");
        this.newPass_t = new JPasswordField(20);   this.sign_in_b = new JButton("Sign In");

        this.roles_cb = new JComboBox<>(roles);

        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        this.setContentPane(content);

        JPanel usernameContent = new JPanel();
        usernameContent.setLayout(new FlowLayout());
        usernameContent.add(new JLabel("Username: "));
        usernameContent.add(usname_t);

        JPanel passwordContent = new JPanel();
        passwordContent.setLayout(new FlowLayout());
        passwordContent.add(new JLabel("Password: "));
        passwordContent.add(pass_t);

        content.add(usernameContent);
        content.add(passwordContent);
        content.add(login_b);
        content.add(new JLabel("Don't have an account?"));
        content.add(sign_up_b);

        this.pack();
        this.addWindowListener(listener);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Log In");
    }
    //Creeam interfata Sign Up
    public void createSignUpFrame(){
        JFrame signUpFrame = new JFrame();
        signUpFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JPanel cont = new JPanel();
        cont.setLayout(new BoxLayout(cont, BoxLayout.PAGE_AXIS));      signUpFrame.setContentPane(cont);


        cont.add(new JLabel("SIGN UP"));

        JPanel newUsernameContent = new JPanel();     newUsernameContent.setLayout(new FlowLayout());

        newUsernameContent.add(new JLabel("Username: "));   newUsernameContent.add(newUsname_t);


        JPanel newPassCont = new JPanel();
        newPassCont.setLayout(new FlowLayout());        newPassCont.add(new JLabel("Password: "));        newPassCont.add(newPass_t);


        cont.add(newUsernameContent);
        cont.add(newPassCont);

        JPanel roleCont = new JPanel();     roleCont.setLayout(new FlowLayout());
        roleCont.add(new JLabel("Role: "));      roleCont.add(roles_cb);


        JPanel securityCode = new JPanel();
        securityCode.setLayout(new FlowLayout());
        securityCode.add(new JLabel("Security code(if administrator or employee): "));
        securityCode.add(code_t);

        cont.add(securityCode);
        cont.add(roleCont);
        cont.add(sign_in_b);

        signUpFrame.pack();
        signUpFrame.setVisible(true);
        signUpFrame.setTitle("Sign Up");
    }


    //Getters and setters pentru actiunile din interfata
    public String getSignUsername(){
        return newUsname_t.getText();
    }

    public char[] getLogPassword(){
        return pass_t.getPassword();
    }


    public String getSelectedRole(){
        return (String) roles_cb.getSelectedItem();
    }
    public char[] getSignPassword(){
        return newPass_t.getPassword();
    }
    public String getLogUsername(){
        return usname_t.getText();
    }

    public String getCode(){
        return code_t.getText();
    }
    public void addSignUpListener(ActionListener sul){
        sign_up_b.addActionListener(sul);
    }

    public void addLogInListener(ActionListener lil){
        login_b.addActionListener(lil);
    }

    public void addSignInListener(ActionListener sl){
        sign_in_b.addActionListener(sl);
    }


    public void showError(String value)
    {
        JOptionPane.showMessageDialog(this, value);
    }
}
