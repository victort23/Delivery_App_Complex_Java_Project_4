package Presentation;

import Data.Serializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.List;
////////////////////////////////DONE
public class AdministratorView extends JFrame {
    private final String username;

    private final JTextField compositeProductName_t = new JTextField(20);   private final JTextField nume_t = new JTextField(20);
    private final JTextField rating_t = new JTextField(5); private final JTextField calorii_t = new JTextField(5);
    private final JTextField proteine_t = new JTextField(5); private final JTextField grasime_t = new JTextField(5);
    private final JTextField sodiu_t = new JTextField(5);private final JTextField pret_t = new JTextField(5);

    private JTable allItems_tb;


    private final JButton addBaseFromTable_b = new JButton("Adauga produs din tabel");
    private final JButton addBaseFromFields_b = new JButton("Adauga produs in lista");    private final JButton delItem_b = new JButton("Sterge item dupa nume");
    private final JButton modItem_b = new JButton("Modifica item dupa nume");    private final JButton addToCompositeTable_b = new JButton("Adauga compus din tabel");
    private final JButton addToCompositeFields_b = new JButton("Adauga compus din campuri");  private final JButton addComposite_b = new JButton("Insereaza item compus");


    private final JTextField BHR_t = new JTextField(3);    private final JTextField AHR_t = new JTextField(3);
    private final JTextField quantityReport_t = new JTextField(5);    private final JTextField countReport_t = new JTextField(5);
    private final JTextField priceReport_t = new JTextField(5);    private final JTextField dateReport_t = new JTextField(10);


    private final JButton rep1_b = new JButton("Genereaza raport!");    private final JButton rep2_b = new JButton("Genereaza raport!");
    private final JButton rep3_b = new JButton("Genereaza raport!"); private final JButton rep4_b = new JButton("Genereaza raport!");


    private JFrame frameAdmin;

    public String getUsername(){
        return username;
    }
    //Interfata pentru administrator
    public AdministratorView(String username){
        this.username = username;

        WindowListener listener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(frameAdmin != null)
                    frameAdmin.dispose();
            }
        };
        this.addWindowListener(listener);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));

        JPanel compositeName = new JPanel();
        compositeName.setLayout(new FlowLayout());
        compositeName.add(new JLabel("Composite Name: "));
        compositeName.add(compositeProductName_t);

        JPanel baseProduct = new JPanel();
        baseProduct.setLayout(new FlowLayout());
        baseProduct.add(new JLabel("Base Name: "));
        baseProduct.add(nume_t);

        JPanel ratingContent = new JPanel();
        ratingContent.setLayout(new FlowLayout());
        ratingContent.add(new JLabel("Rating: "));
        ratingContent.add(rating_t);

        JPanel caloriesContent = new JPanel();
        caloriesContent.setLayout(new FlowLayout());
        caloriesContent.add(new JLabel("Calories: "));
        caloriesContent.add(calorii_t);


        JPanel criteria1 = new JPanel();
        criteria1.setLayout(new BoxLayout(criteria1, BoxLayout.PAGE_AXIS));
        criteria1.add(ratingContent);
        criteria1.add(caloriesContent);

        JPanel proteinContent = new JPanel();
        proteinContent.setLayout(new FlowLayout());
        proteinContent.add(new JLabel("Protein: "));
        proteinContent.add(proteine_t);

        JPanel fatContent = new JPanel();
        fatContent.setLayout(new FlowLayout());
        fatContent.add(new JLabel("Fat: "));
        fatContent.add(grasime_t);


        JPanel criteria2 = new JPanel();
        criteria2.setLayout(new BoxLayout(criteria2, BoxLayout.PAGE_AXIS));
        criteria2.add(proteinContent);
        criteria2.add(fatContent);

        JPanel sodiumContent = new JPanel();
        sodiumContent.setLayout(new FlowLayout());
        sodiumContent.add(new JLabel("Sodium: "));
        sodiumContent.add(sodiu_t);

        JPanel priceContent = new JPanel();
        priceContent.setLayout(new FlowLayout());
        priceContent.add(new JLabel("Price"));
        priceContent.add(pret_t);


        JPanel criteria3 = new JPanel();
        criteria3.setLayout(new BoxLayout(criteria3, BoxLayout.PAGE_AXIS));
        criteria3.add(sodiumContent);
        criteria3.add(priceContent);

        JPanel crit = new JPanel();
        crit.setLayout(new FlowLayout());   crit.add(criteria1);

        crit.add(criteria2);  crit.add(criteria3);


        JPanel buttonSet1 = new JPanel();   buttonSet1.setLayout(new FlowLayout());

        buttonSet1.add(delItem_b);   buttonSet1.add(modItem_b);


        JPanel buttonSet2 = new JPanel();        buttonSet2.setLayout(new FlowLayout());

        buttonSet2.add(addBaseFromFields_b);        buttonSet2.add(addBaseFromTable_b);


        JPanel buttonSet3 = new JPanel();    buttonSet3.setLayout(new FlowLayout());

        buttonSet3.add(addToCompositeFields_b);      buttonSet3.add(addToCompositeTable_b);


        JPanel report1 = new JPanel();
        report1.setLayout(new FlowLayout());    report1.add(new JLabel("Time intervals for the report: between "));

        report1.add(BHR_t);        report1.add(new JLabel(" and "));

        report1.add(AHR_t);        report1.add(rep1_b);


        JPanel report2 = new JPanel();
        report2.setLayout(new FlowLayout());     report2.add(new JLabel("The quantity for the second report: "));

        report2.add(quantityReport_t);       report2.add(rep2_b);


        JPanel report3 = new JPanel();
        report3.setLayout(new FlowLayout()); report3.add(new JLabel("Minimum number of orders: "));
        report3.add(countReport_t);     report3.add(new JLabel(" with the minimum price: "));

        report3.add(priceReport_t);       report3.add(rep3_b);


        JPanel report4 = new JPanel();
        report4.setLayout(new FlowLayout());      report4.add(new JLabel("Day of the orders: "));

        report4.add(dateReport_t);   report4.add(rep4_b);

        content.add(compositeName);   content.add(baseProduct);

        content.add(crit);    content.add(buttonSet1);

        content.add(buttonSet2);       content.add(buttonSet3);

        content.add(addComposite_b);    content.add(report1);

        content.add(report2);  content.add(report3);


        this.setContentPane(content);
        this.pack();
        this.setTitle("Administrator View");
    }

    public void createTable(List<Business.MenuItem> items)
    {
        int koef = 0;
        String[] columnNames ={ "Nume",
                "Rating",
                "Calorii",
                "Proteine",
                "Grasime",
                "Sodiu",
                "Pret"};
        Object[][] data = new Object[items.size()][7];

        for(Business.MenuItem item: items){
            data[koef][0] = item.getName();
            data[koef][1] = item.getRating();
            data[koef][2] = item.getCalories();
            data[koef][3] = item.getProtein();
            data[koef][4] = item.getFat();
            data[koef][5] = item.getSodium();
            data[koef][6] = item.getPrice();
            koef++;
        }

        allItems_tb = new JTable(data, columnNames);
        JScrollPane scrollPane = new JScrollPane(allItems_tb);
        allItems_tb.setFillsViewportHeight(true);

        frameAdmin = new JFrame();
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(scrollPane);

        frameAdmin.setContentPane(panel);
        frameAdmin.pack();
        frameAdmin.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frameAdmin.setVisible(true);
        frameAdmin.setTitle("All items - Administrator View");
    }
    public void addBaseFromTableListener(ActionListener a){
        addBaseFromTable_b.addActionListener(a);
    }
    public void addDeleteItemListener(ActionListener a){
        delItem_b.addActionListener(a);
    }

    public void addModifyItemListener(ActionListener a){
        modItem_b.addActionListener(a);
    }
    public void addRep3Listener(ActionListener a){
        rep3_b.addActionListener(a);
    }


    public void addBaseFromFieldsListener(ActionListener a){
        addBaseFromFields_b.addActionListener(a);
    }

    public void addToCompositeFieldsListener(ActionListener a){
        addToCompositeFields_b.addActionListener(a);
    }
    public void addRep4Listener(ActionListener a){
        rep4_b.addActionListener(a);
    }

    public void addToCompositeTableListener(ActionListener a){
        addToCompositeTable_b.addActionListener(a);
    }
    public void addRep2Listener(ActionListener a){
        rep2_b.addActionListener(a);
    }

    public void addCompositeListener(ActionListener a){
        addComposite_b.addActionListener(a);
    }

    public void addRep1Listener(ActionListener a){
        rep1_b.addActionListener(a);
    }







    public String getCompositeText(){
        return compositeProductName_t.getText();
    }

    public String getBaseText(){
        return nume_t.getText();
    }

    public String getRatingText(){
        return rating_t.getText();
    }

    public String getCaloriesTest(){
        return calorii_t.getText();
    }

    public String getProteinText(){
        return proteine_t.getText();
    }

    public String getFatText(){
        return grasime_t.getText();
    }

    public String getSodiumText(){
        return sodiu_t.getText();
    }

    public String getPriceText(){
        return pret_t.getText();
    }

    public String getBeforeHourText(){
        return BHR_t.getText();
    }

    public String getAfterHourText(){
        return AHR_t.getText();
    }

    public String getQuantityText(){
        return quantityReport_t.getText();
    }

    public String getCountText(){
        return countReport_t.getText();
    }

    public String getPriceReportText(){
        return priceReport_t.getText();
    }

    public String getDateText(){
        return dateReport_t.getText();
    }

    public int getIndexSelectedRow(){
        return allItems_tb.getSelectedRow();
    }

    public void disposeTable(){
        frameAdmin.dispose();
    }

    public void showMessage(String value)
    {
        JOptionPane.showMessageDialog(this, value);
    }
}
